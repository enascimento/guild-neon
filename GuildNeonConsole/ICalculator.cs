﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuildNeonConsole
{
    public interface ICalculator
    {
        int Sum(int a, int b);

        void Print(string message);
    }
}
