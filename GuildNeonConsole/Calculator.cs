﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuildNeonConsole
{
    public class Calculator : ICalculator
    {

        public int Sum(int a, int b)
        {
            return a + b;
        }


        public void Print(string message)
        {
            Console.WriteLine(message);
            Console.WriteLine("OK");
        }
    }
}
