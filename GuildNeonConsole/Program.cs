﻿using System;

namespace GuildNeonConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Wait();
        }

        private static void Wait()
        {
            Console.Read();
        }

        private static void Write(string message)
        {
            Console.WriteLine(message);
        }

    }
}
